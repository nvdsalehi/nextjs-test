module.exports = {
  env: { browser: true, node: true, 'jest/globals': true },
  extends: [
    'airbnb', 'airbnb/hooks',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'jest',
  ],
  rules: {
    'no-unused-vars': 'warn',
  },
};
